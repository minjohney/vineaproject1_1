package member;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class mManager {

	// 재능기부 데이터 베이스 접속을 위한 변수선언
	Connection conn = null;
	PreparedStatement pstmt = null;
	String jdbc_driver = "org.postgresql.Driver";
	String jdbc_url = "jdbc:postgresql://localhost:5432/toolsup";

	// 재능기부 데이터 베이스 접속
	void connect() {
		try {
			Class.forName(jdbc_driver);

			conn = DriverManager.getConnection(jdbc_url, "postgres", "1234");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 재능기부 데이터 베이스 접속 해제
	void disConnect() {
		if (pstmt != null) {
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean idcheck(String id)
	{
		connect();
		
		String sql = "select userid from member where userid = ?";
		boolean result = false;
		ResultSet rs = null;

		
		try
		{
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			
			if(rs.next())
			{
				result = true;
			}
			else
			{
				result = false;
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			disConnect();
		}
		return result;
	}

	public boolean login(String id, String pw) {
		connect();
		String sql = "select userid, passwd from member where userid = ?";
		String pass;

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			pass = rs.getString("passwd");
			rs.close();
			if (pass.equals(pw)) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			disConnect();
		}
		return false;
	}

	public boolean join(mBean user1) {
		connect();

		String sql = "insert into member(username, userid, passwd, email, tel) values(?,?,?,?,?)";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, user1.getUsername());
			pstmt.setString(2, user1.getUserid());
			pstmt.setString(3, user1.getPasswd());
			pstmt.setString(4, user1.getEmail());
			pstmt.setString(5, user1.getTel());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			disConnect();
		}
		return true;
	}

}
